import { Injectable } from '@angular/core';
import { User } from '../model/user.model';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private userIsLogged = new BehaviorSubject<boolean>(false);
  currentUserIsLogged = this.userIsLogged.asObservable();
  
  constructor(private router: Router) { }

  signin(user: User) {
    if (user.username === 'a' && user.password === '123456') {
      this.changeUserIsLogged(true);
      this.router.navigate(['']);
    } else {
      this.changeUserIsLogged(false);
    }
  }

  signout() {

  }

  signup() {

  }

  changeUserIsLogged(value: boolean) {
    this.userIsLogged.next(value);
  }

  get getUserIsLogged(): boolean {
    return this.userIsLogged.value;
  }
}
