import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './auth.service';
import { User } from '../model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;



  user: User;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder) {

    this.loginForm = this.fb.group({
      UserName: ['', Validators.required],
      Password: ['', Validators.required]
    });

  }

  public submit(): void {
    if (this.loginForm.valid) {
      
      this.user = {
        username: this.loginForm.value.UserName,
        password: this.loginForm.value.Password
      };

      this.authService.signin(this.user);

    }
  }

}
