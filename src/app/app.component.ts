import { Component, OnInit } from '@angular/core';
import { AuthService } from './login/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'meat-frontend';
  content = 'Welcome do Meat App!';

  constructor(private authService: AuthService) {}

  userIsLogged = false;

  ngOnInit() {
    this.authService.currentUserIsLogged
      .subscribe(response => this.userIsLogged = response);
  }
}
